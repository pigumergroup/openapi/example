package com.pigumer.example.controller.api;

import com.pigumer.example.controller.model.Hello;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloApiImpl implements HelloApi {

    @Override
    public ResponseEntity<Hello> helloGet() {
        Hello hello = new Hello();
        hello.setMessage("Hello World!");
        return new ResponseEntity<>(hello, HttpStatus.OK);
    }
}
