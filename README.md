Example
=======

# 生成されたモジュールのビルド

```
$ cd example-service-generated-modules
$ docker-compose up
$ docker-compose down
$ mvn clean install
```

# APIのビルドと実行

```
$ cd example-service-api
$ mvn clean spring-boot:run
```

ブラウザで次のURLにアクセス:
* http://localhost:8080
* http://localhost:8080/hello
